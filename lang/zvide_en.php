<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zvide?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_zvide' => 'Header and footer',

	// D
	'description_page_article' => 'Default page for articles.',
	'description_page_auteur' => 'Default page for authors.',
	'description_page_breve' => 'Default page for news.',
	'description_page_evenement' => 'Default page for events.',
	'description_page_groupe_mots' => 'Optional page for groups of keywords.',
	'description_page_mot' => 'Default page for keywords.',
	'description_page_rubrique' => 'Default page for sections.',
	'description_page_site' => 'Default page for referenced websites.',
	'description_pagedefaut' => 'Blocks of this page will be added to all pages of the website.',

	// E
	'explication_liens_add' => 'You can enter one or more additional links to put in the footers. If you add more links, remember to separate them with a |. You can use SPIP shortcuts. For example: <code>[Contact->12] | [Legal statements->art13]</ code>',
	'explication_masquer_connexion' => 'Hide links to connect / disconnect?',
	'explication_masquer_logo' => 'Hide the logo of the website ?',
	'explication_masquer_plan' => 'Hide the link to access to the site map?',
	'explication_masquer_rss' => 'Hide the link to the website’s RSS feed?',
	'explication_masquer_slogan' => 'Hide the slogan of the website ?',
	'explication_menu_lang' => 'This option affects only multilingual sites.<br/>The option <em>Default</ em> reproduces the Zpip-dist function : a form of language choices is displayed on all pages. When a language is selected by the user, the page is reloaded by passing a parameter <code>lang</code>. This operation is suitable for multilingual sites using blocks (<code><multi></code>) in the editorial objects and having set the global variable with <code>forcer_lang</code> at <code>true</code>.<br/> <em>Home only</em> will display the language selection form only on the home page. <br/> <em>Back to Homepage</em> will display the form on all pages, but the choice of language will return to the home page in the selected language.<br/> Finally, the option <em>Links</em> corresponds to sites using translation links between articles. Form choice of language will be displayed on pages that do not correspond to an editorial object (homepage, site map, etc.).. On the pages of articles, the form will be displayed if translations are available and pointing to these translations. The operation will be equivalent to the pages of sections if the plugin <em>trad_rub</ em> is installed.',

	// L
	'label_choix_menu_lang_defaut' => 'Default',
	'label_choix_menu_lang_liens_trad' => 'Translation links',
	'label_choix_menu_lang_masquer' => 'Hide on all pages',
	'label_choix_menu_lang_retour_sommaire' => 'Back to homepage',
	'label_choix_menu_lang_sommaire' => 'Homepage only',
	'label_liens_add' => 'Additional links',
	'label_masquer_connexion' => 'Login link',
	'label_masquer_logo' => 'Website logo',
	'label_masquer_plan' => 'Sitemap',
	'label_masquer_rss' => 'RSS Feeds',
	'label_masquer_slogan' => 'Website slogan',
	'label_menu_lang' => 'Language menu',
	'label_options_en_tete' => 'Options in the header page',
	'label_options_pied' => 'Options in the footer page',
	'label_taille_logo' => 'Maximum size of the logo in pixels',

	// N
	'nom_page_article' => 'Article',
	'nom_page_auteur' => 'Author',
	'nom_page_breve' => 'News',
	'nom_page_evenement' => 'Event',
	'nom_page_groupe_mots' => 'Group of keywords',
	'nom_page_mot' => 'Keyword',
	'nom_page_rubrique' => 'Section',
	'nom_page_site' => 'Referenced website',
	'nom_pagedefaut' => 'Default page',

	// Z
	'zvide' => 'Empty Zpip'
);
