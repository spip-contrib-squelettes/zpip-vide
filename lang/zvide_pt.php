<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zvide?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_zvide' => 'Cabeçalho e rodapé',

	// D
	'description_page_article' => 'Página padrão para artigos.',
	'description_page_auteur' => 'Página padrão para autores.',
	'description_page_breve' => 'Página padrão para as breves.',
	'description_page_evenement' => 'Página padrão para eventos.',
	'description_page_groupe_mots' => 'Página opcional para grupos de palavras-chave.',
	'description_page_mot' => 'Página padrão para palavras-chave.',
	'description_page_rubrique' => 'Página padrão para as rubricas.', # RELIRE
	'description_page_site' => 'Página padrão para sítios referenciados.',
	'description_pagedefaut' => 'Os blocos desta página serão adicionados a todas as páginas do sítio.',

	// E
	'explication_liens_add' => 'Você pode inserir aqui uma ou mais ligações adicionais para colocar no rodapé. Se você adicionar várias ligações, considere separá-las com um |. Você pode usar os atalhos SPIP. Por exemplo :<code>[Contact->12] | [Mentions légales->art13]</code>',
	'explication_masquer_connexion' => 'Ocultar ligações para conectar / desconectar?',
	'explication_masquer_logo' => 'Ocultar o logotipo do sítio ?',
	'explication_masquer_plan' => 'Ocultar a ligação para o mapa do sítio?', # RELIRE
	'explication_masquer_rss' => 'Ocultar a ligação que aponta para o feed RSS do sítio ?', # RELIRE
	'explication_masquer_slogan' => 'Ocultar slõgane do sítio ?',
	'explication_menu_lang' => 'Esta opção afeta apenas sítios multilíngues.<br />A opção <em>Padrão</em> reproduz a função Zpip-dist : Um formulário de escolha de idioma é exibido em todas as páginas. Quando um idioma é selecionado pelo usuário, a página é recarregada passando-lhe um parâmetro <code>lang</code>. Esta operação é adequada para sítios que usam blocos multilíngues. (<code><multi></code>) em objetos editoriais e tendo definida a variável global <code>forcer_lang</code> a <code>true</code>.<br />A opção <em>Página inicial unicamente</em> irá mostrar o formulário de seleção de idioma apenas na página inicial.<br />A opção <em>Voltar á página inicial</em> irá exibir o formulário em todas as páginas, mas a escolha de um idioma irá retornar para a página inicial no idioma escolhido.<br />Enfim, A opção <em>Ligações de tradução</em> corresponde a sítios que usam ligações de tradução entre artigos. O formulário de escolha do idioma só será exibido em páginas que não correspondam a um objeto editorial (início, mapa do sítio, etc.). Nas páginas dos artigos, o formulário será exibido se houver traduções disponíveis e apontará para essas traduções. A operação será equivalente nas páginas das seções se o plugin <em>trad_rub</em> estiver instalado.', # RELIRE

	// L
	'label_choix_menu_lang_defaut' => 'Padrão',
	'label_choix_menu_lang_liens_trad' => 'Ligaçõrs de tradução',
	'label_choix_menu_lang_masquer' => 'Ocultar em todas as páginas',
	'label_choix_menu_lang_retour_sommaire' => 'Voltar á página inicial',
	'label_choix_menu_lang_sommaire' => 'Página inicial apenas',
	'label_liens_add' => 'Ligações adicionais',
	'label_masquer_connexion' => 'Ligação de conexão',
	'label_masquer_logo' => 'Logotipo do sítio',
	'label_masquer_plan' => 'Mapa do sítio',
	'label_masquer_rss' => 'Fluxo RSS', # RELIRE
	'label_masquer_slogan' => 'Slogâne do sítio',
	'label_menu_lang' => 'Menu de idiomas',
	'label_options_en_tete' => 'Opções de cabeçalho de página',
	'label_options_pied' => 'Opções de rodapé',
	'label_taille_logo' => 'Tamanho máximo do logotipo em pixels',

	// N
	'nom_page_article' => 'Artigo', # RELIRE
	'nom_page_auteur' => 'Autor',
	'nom_page_breve' => 'Breve', # RELIRE
	'nom_page_evenement' => 'Evento',
	'nom_page_groupe_mots' => 'Grupo de palavras-chave',
	'nom_page_mot' => 'Palavra-Chave',
	'nom_page_rubrique' => 'Rubriqua', # RELIRE
	'nom_page_site' => 'Site referenciado', # RELIRE
	'nom_pagedefaut' => 'Página padrão',

	// Z
	'zvide' => 'Zpip-vide' # RELIRE
);
