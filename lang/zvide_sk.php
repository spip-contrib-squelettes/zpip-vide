<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zvide?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_zvide' => 'Hlavička a päta stránky',

	// D
	'description_page_article' => 'Predvolená stránka pre články.',
	'description_page_auteur' => 'Predvolená stránka pre autorov.',
	'description_page_breve' => 'Predvolená stránka pre novinky.',
	'description_page_evenement' => 'Predvolená stránka pre udalosti.',
	'description_page_groupe_mots' => 'Nepovinná stránka pre skupiny kľúčových slov.',
	'description_page_mot' => 'Predvolená stránka pre kľúčové slová.',
	'description_page_rubrique' => 'Predvolená stránka pre rubriky.',
	'description_page_site' => 'Predvolená stránka pre webové stránky, na ktoré odkazujete.',
	'description_pagedefaut' => 'Bloky tejto stránky budú pridané na všetky stránky tohto webu.',

	// E
	'explication_liens_add' => 'Sem môžete zadať jeden alebo viac odkazov, ktoré budú vložené do päty stránky. Ak vložíte viac odkazov, nezabudnite ich oddeliť znakom |. Môžete použiť skratky SPIPu. Napríklad: <code>[Kontakt->12] | [Informácie právneho charakteru->art13]</code>',
	'explication_masquer_connexion' => 'Schovať odkazy umožňujúce prihlásiť sa/odhlásiť sa?',
	'explication_masquer_logo' => 'Schovať logo stránky?',
	'explication_masquer_plan' => 'Schovať odkaz na mapu stránky?',
	'explication_masquer_rss' => 'Schovať odkaz na kanál RSS?',
	'explication_masquer_slogan' => 'Schovať slogan stránky?',
	'explication_menu_lang' => 'Táto voľba ovplyvní iba viacjazyčné stránky.<br />Možnosť <em>Predvolené</em> reprodukuje funkciu Zpip-dist: formulár s výberom jazyka sa zobrazí na všetkých stránkach. Ak si jazyk zvolí používateľ, stránka sa načíta s parametrom <code>lang.</code> Toto nastavenie je vhodné pre stránky, ktoré v redakčných objektoch využívajú viacjazyčné bloky(<code><multi></code>) a ktoré definujú globálnu premennú <code>forcer_lang</code> ako <code>true.</code><br />Možnosť <em>Iba úvodná stránka</em> zobrazí formulár s výberom jazyka iba na úvodnej stránke.<br />Možnosť <em>Vrátiť sa na úvodnú stránku</em> zobrazí formulár na všetkých stránkach, ale výber jazyka bude presmerovaný na úvodnú stránku vo zvolenom jazyku.<br />Napokon možnosť <em>Odkazy na preklad</em> je rovnaká ako pri stránkach, ktoré využívajú odkazy na preklady článkov. Formulár s výberom jazyka sa zobrazí iba na stránkach, ktoré nesúvisia so žiadnym redakčným objektom (úvodná stránka, mapa stránky, atď.). Na stránkach s článkami sa formulár zobrazí v prípade, že sú preklady k dispozícii, a bude na tieto preklady odkazovať. Jeho fungovanie je podobné ako na stránkach rubrík, ak je nainštalovaný zásuvný modul <em>trad_rub.</em>',

	// L
	'label_choix_menu_lang_defaut' => 'Predvolený',
	'label_choix_menu_lang_liens_trad' => 'Odkazy na preklad',
	'label_choix_menu_lang_masquer' => 'Schovať všetky stránky',
	'label_choix_menu_lang_retour_sommaire' => 'Vrátiť sa na úvodnú stránku',
	'label_choix_menu_lang_sommaire' => 'Iba úvodná stránka',
	'label_liens_add' => 'Ďalšie odkazy',
	'label_masquer_connexion' => 'Odkaz na prihlásenie sa',
	'label_masquer_logo' => 'Logo stránky',
	'label_masquer_plan' => 'Mapa stránky',
	'label_masquer_rss' => 'Kanál RSS',
	'label_masquer_slogan' => 'Slogan stránky',
	'label_menu_lang' => 'Jazykové menu',
	'label_options_en_tete' => 'Možnosti hlavičky stránky',
	'label_options_pied' => 'Možnosti päty stránky',
	'label_taille_logo' => 'Maximálna veľkosť loga v pixeloch',

	// N
	'nom_page_article' => 'Článok',
	'nom_page_auteur' => 'Autor',
	'nom_page_breve' => 'Novinka',
	'nom_page_evenement' => 'Udalosť',
	'nom_page_groupe_mots' => 'Skupina kľúčových slov',
	'nom_page_mot' => 'Kľúčové slovo',
	'nom_page_rubrique' => 'Rubrika',
	'nom_page_site' => 'Stránka, na ktorú odkazujete',
	'nom_pagedefaut' => 'Predvolená stránka',

	// Z
	'zvide' => 'Zpip-vide'
);
