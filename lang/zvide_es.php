<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zvide?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_zvide' => 'Cabecera y pie de página', # he traducido en-tête por cabecera, si alguno tiene una idea mejor...

	// D
	'description_page_article' => 'Página por defecto para los artículos.',
	'description_page_auteur' => 'Página por defecto para los autores.',
	'description_page_breve' => 'Página por defecto para las breves.',
	'description_page_evenement' => 'Página por defecto para los acontecimientos.',
	'description_page_groupe_mots' => 'Página facultativa para los grupos de palabras clave.',
	'description_page_mot' => 'Página por defecto para las palabras-claves',
	'description_page_rubrique' => 'Página por defecto para las rúbricas.',
	'description_page_site' => 'Página por defecto para los sitios web referenciados.',
	'description_pagedefaut' => 'Los blocs de esta página serán añadidos a todas las páginas del sitio.',

	// E
	'explication_liens_add' => 'Usted puede  insertar aquí  uno o varios vínculos adicionales a poner en el pié de la página. Si usted añade varios vínculos, piense a separarlos con un |. Puede utilizar los atajos spip. Por ejemplo :<code>[Contact->12] | [Mentions légales->art13]</code>


',
	'explication_masquer_connexion' => 'Ocultar los vínculos  que permiten conectarse / desconectarse ?',
	'explication_masquer_logo' => 'Ocultar el logo del sitio ?',
	'explication_masquer_plan' => 'Ocultar el vínculo de acceso al pla del sitio ?l',
	'explication_masquer_rss' => 'Ocultar el vínculo que apunta al flujo RSS del sitio ? ',
	'explication_masquer_slogan' => 'Ocultar el eslogan del sitio web ? ',
	'explication_menu_lang' => 'Esta opción solo afecta los sitios webs multilingüísticos.<br />La opción<em>Defecto</em> reproduce la funcción de Zpip-dist : un formulario de elección de lengua es mostrado en todas las páginas. Cuando una lengua es seleccionada por el usuario, la página es recargada  pasándole un parámetro <code>lang</code>. Este funcionamiento se adapta al funcionamiento de los sitios que utilizan blocs multilingüísticos  (<code><multi></code>) en los objetos editoriales y que tengan definida  la variable global  <code>forcer_lang</code> a <code>true</code>.<br />La opción<em>Páginade inicsolamente</em> mostrará el formulario de selección de lengua unicamente en la página de inicio .<br />La opción<em>volver a la página de inicio</em> mostrará el formulario en todas las páginas, pero la elección de una lengua   implicará  el retorno  a la página de inicio en la lengua elegida.<br />Enfin, la opción <em>Vínculos de traducción</em> corresponde a los  sitios utilizando vínculos de traducción entre artículos.  El formulario de elección de la lengua  solo  será mostrado en las páginas que no correspondan a un objeto editorial (inicio, plan del sitio, etc.). En las páginas de los artículos, el formulario será mostrado si hay traducciones disponibles y apuntará a dichas traducciones.   El funcionamiento  será equivalente en  las páginas de rúbricas si el plugin <em>trad_rub</em> está instalado.',

	// L
	'label_choix_menu_lang_defaut' => 'Defecto',
	'label_choix_menu_lang_liens_trad' => 'vínculos de traducción',
	'label_choix_menu_lang_masquer' => 'Ocultar en todas las páginas',
	'label_choix_menu_lang_retour_sommaire' => 'Volver a la página de  inicio',
	'label_choix_menu_lang_sommaire' => 'Página de inicio  solamente',
	'label_liens_add' => 'Vínculos adicionales',
	'label_masquer_connexion' => 'Vínculo de conexión ',
	'label_masquer_logo' => 'Logo del  sitio',
	'label_masquer_plan' => 'Plan del  sitio',
	'label_masquer_rss' => 'Flujo RSS',
	'label_masquer_slogan' => 'Slogan del sitio',
	'label_menu_lang' => 'Menu de lenguas',
	'label_options_en_tete' => 'Opciones de la cabecera de página',
	'label_options_pied' => 'Opciones del  pie de página',
	'label_taille_logo' => 'Tamaño  máximo del logo en pixels',

	// N
	'nom_page_article' => 'Artículo',
	'nom_page_auteur' => 'Autor',
	'nom_page_breve' => 'Breve',
	'nom_page_evenement' => 'Acontecimiento',
	'nom_page_groupe_mots' => 'Grupo de palabras-clave',
	'nom_page_mot' => 'Palabra-clave',
	'nom_page_rubrique' => 'Rúbrica',
	'nom_page_site' => 'Sitio referenciado',
	'nom_pagedefaut' => 'Página por defecto',

	// Z
	'zvide' => 'Zpip-vide'
);
